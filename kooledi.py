#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''

* a small, 400 lines of code, single-file, text editor and python-launcher (1 button run without manual save).
may come in handier than idle3 where you have to press many more buttons...

* Uses OS-native file dialogues, such as the beautiful and ergonomic KDE filepicker though the GTK3 library is being used as the basis. 

* launches straight from KDE-Dolphin click, KMenu & sidebar. Opens a called %f  filename from cmd line.

* Can be expanded on to make a full IDElike "idle3" based on GTK3. Just edit this very python source file and press the run button for testing.

* launch 2 new instances of kooledi to use drag'n'drop of text. 
  A parent kooledi launched via run-button cannot drop from its child instance though.




Very few dependencies.  uses: python3.7 , gtk3  but not much else.


TODO:    
* set filters to a fixed choice (change order?)
* mark content dirty
* 



    py2 and py3 poss  ( py2 you need to adapt the launcher yourself...  just a single popen() line  ;-)    )

https://developer.gnome.org/gtk3/stable/gtk3-GtkFileChooserNative.html#gtk-file-chooser-native-new

https://stackoverflow.com/questions/48750086/how-can-i-use-filechoosernative-from-pygtk3

https://www.heise.de/forum/p-33951048/

https://lazka.github.io/pgi-docs/Gtk-3.0/classes/TextBuffer.html#Gtk.TextBuffer.get_end_iter

apt install gobject  ...  bla bla



'''
 
import sys,os,gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Pango

os.environ["GTK_USE_PORTAL"] = "1"    


class SearchDialog(Gtk.Dialog):

    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Search", parent,
            Gtk.DialogFlags.MODAL, buttons=(
            Gtk.STOCK_FIND, Gtk.ResponseType.OK,
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL))

        box   = self.get_content_area()
        label = Gtk.Label("Insert text to search for:")
        box.add(label)
        e=      self.entry = Gtk.Entry()
        box.add(self.entry)
        e=      self.show_all()




class koolEdi(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title=" kool Edi 5 ")
        self.set_default_size(800, 500)
        self.grid = Gtk.Grid()
        self.add(self.grid)
        self.create_textview()
        self.create_toolbar()
        self.create_buttons()


    ###################################################################################

    

    ''' save as temp and launch it with py3 '''
    def on_button_lauf(self, widget,       tag):
        try:
            ei=self.textbuffer.get_end_iter()
            si=self.textbuffer.get_iter_at_line(0)
            vt=self.textbuffer.get_text(si,ei,True) 
            with open( "tem.py" , 'w' ) as f:          e = f.write(vt)             
        except: pass
        os.spawnlp( os.P_WAIT, 'python3', 'python3', 'tem.py'    , '####kpara')
        #os.spawnlp(os.P_WAIT, 'cp'     , 'cp'     , 'index.html', '/dev/null')














    def create_toolbar(self):
        toolbar = Gtk.Toolbar()
        self.grid.attach(toolbar, 0, 0, 3, 1)
        
         
        ##############################################################################document-open
        toolbar.insert(Gtk.SeparatorToolItem(), 12)
        
        button_open = Gtk.ToolButton()
        button_open.set_icon_name("document-open")              #"computer")   open-menu-symbolic  media-eject-symbolic                                                     
        toolbar.insert(button_open, 13)
        #button_bold.connect(     "clicked" , self.on_button_clicked,               self.tag_bold)
        button_open.connect(      "clicked" , self.on_button_op     ,               self.tag_open)
       



        button_save = Gtk.ToolButton()
        button_save.set_icon_name("document-save-as")
        toolbar.insert(button_save, 14)
        button_save.connect(      "clicked" , self.on_button_savas  ,               self.tag_open)

        toolbar.insert(Gtk.SeparatorToolItem(), 15)



        button_lauf = Gtk.ToolButton()
        button_lauf.set_icon_name("media-playback-start")
        toolbar.insert(button_lauf, 16)
        button_lauf.connect(      "clicked" , self.on_button_lauf  ,               self.tag_open)





        
        #############################################################################
       

        button_bold = Gtk.ToolButton()
        button_bold.set_icon_name("format-text-bold-symbolic")
        toolbar.insert(button_bold, 0)

        button_italic = Gtk.ToolButton()
        button_italic.set_icon_name("format-text-italic-symbolic")
        toolbar.insert(button_italic, 1)

        button_underline = Gtk.ToolButton()
        button_underline.set_icon_name("format-text-underline-symbolic")
        toolbar.insert(button_underline, 2)

       

        button_bold.connect(     "clicked", self.on_button_clicked,               self.tag_bold)
        button_italic.connect(   "clicked", self.on_button_clicked,             self.tag_italic)
        button_underline.connect("clicked", self.on_button_clicked,          self.tag_underline)

        toolbar.insert(Gtk.SeparatorToolItem(), 3)

        radio_justifyleft = Gtk.RadioToolButton()
        radio_justifyleft.set_icon_name("format-justify-left-symbolic")
        toolbar.insert(radio_justifyleft, 4)

        radio_justifycenter = Gtk.RadioToolButton.new_from_widget(radio_justifyleft)
        radio_justifycenter.set_icon_name("format-justify-center-symbolic")
        toolbar.insert(radio_justifycenter, 5)

        radio_justifyright = Gtk.RadioToolButton.new_from_widget(radio_justifyleft)
        radio_justifyright.set_icon_name("format-justify-right-symbolic")
        toolbar.insert(radio_justifyright, 6)

        radio_justifyfill = Gtk.RadioToolButton.new_from_widget(radio_justifyleft)
        radio_justifyfill.set_icon_name("format-justify-fill-symbolic")
        toolbar.insert(radio_justifyfill, 7)

        radio_justifyleft.connect("toggled", self.on_justify_toggled,         Gtk.Justification.LEFT)
        radio_justifycenter.connect("toggled", self.on_justify_toggled,       Gtk.Justification.CENTER)
        radio_justifyright.connect("toggled", self.on_justify_toggled,        Gtk.Justification.RIGHT)
        radio_justifyfill.connect("toggled", self.on_justify_toggled,         Gtk.Justification.FILL)

        toolbar.insert(Gtk.SeparatorToolItem(), 8)

        button_clear = Gtk.ToolButton()
        button_clear.set_icon_name("edit-clear-symbolic")
        button_clear.connect("clicked", self.on_clear_clicked)
        toolbar.insert(button_clear, 9)

        toolbar.insert(Gtk.SeparatorToolItem(), 10)

        button_search = Gtk.ToolButton()
        button_search.set_icon_name("system-search-symbolic")
        button_search.connect("clicked", self.on_search_clicked)
        toolbar.insert(button_search, 11)




    ######################################################################################################




    def on_button_savas(self, widget,       tag):
        dialog         = Gtk.FileChooserNative()
        e              = self.add_filters(dialog)
        response       =                  dialog.run()
        if   response == Gtk.ResponseType.CANCEL:                   pass
        elif response == Gtk.ResponseType.OK or True :            
            ei=self.textbuffer.get_end_iter()
            si=self.textbuffer.get_iter_at_line(0)
            vt=self.textbuffer.get_text(si,ei,True) 
            with open( dialog.get_filename() , 'w' ) as f:          e = f.write(vt)  
        e    =dialog.destroy()          
        
  




    def on_button_op(self, widget, tag):
        print("Open 1")
        dialog   = Gtk.FileChooserNative()
        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.CANCEL:
            pass
            print("Cancel clicked")

        elif response == Gtk.ResponseType.OK or True :
            print("File selected: " + dialog.get_filename())

            
            with open( dialog.get_filename() ) as f:
                inh = f.read()
            

            self.textbuffer.set_text( inh )          

        dialog.destroy()





    def add_filters(self, dialog):
        filter_text = Gtk.FileFilter()
        filter_text.set_name("Text files")
        filter_text.add_mime_type("text/plain")
        dialog.add_filter(filter_text)

        filter_py = Gtk.FileFilter()
        filter_py.set_name("Python files")
        filter_py.add_mime_type("text/x-python")
        dialog.add_filter(filter_py)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)













    def create_textview(self):
        global scrolledwindow
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        self.grid.attach(scrolledwindow, 0, 1, 3, 1)



        self.textview = Gtk.TextView()
        self.textbuffer = self.textview.get_buffer()








        try:
            dn=sys.argv[0]
            p1=str(sys.argv[1])
        
            with open( p1 ) as f:
                 inh = f.read()          
            self.textbuffer.set_text( inh )          
        except: pass












        #self.textbuffer.set_text(str(dn) + " - " + str(p1)  )
        #self.textbuffer.set_text("  \n\n\n      \n\n    Gtk.TextView. "
        #    +                    "                      ---   ")

        scrolledwindow.add(self.textview)



        self.tag_open = 1 

        self.tag_bold      = self.textbuffer.create_tag("bold",                 weight=Pango.Weight.BOLD)
        self.tag_italic    = self.textbuffer.create_tag("italic",               style=Pango.Style.ITALIC)
        self.tag_underline = self.textbuffer.create_tag("underline",    underline=Pango.Underline.SINGLE)
        self.tag_found     = self.textbuffer.create_tag("found",                     background="yellow")
        
        
        
        
        

    def create_buttons(self):
        check_editable = Gtk.CheckButton("Editable")
        check_editable.set_active(True)
        check_editable.connect("toggled", self.on_editable_toggled)
        self.grid.attach(check_editable, 0, 2, 1, 1)

        check_cursor = Gtk.CheckButton("Cursor Visible")
        check_cursor.set_active(True)
        check_editable.connect("toggled", self.on_cursor_toggled)
        self.grid.attach_next_to(check_cursor, check_editable,
            Gtk.PositionType.RIGHT, 1, 1)

        radio_wrapnone = Gtk.RadioButton.new_with_label_from_widget(None,
            "No Wrapping")
        self.grid.attach(radio_wrapnone, 0, 3, 1, 1)

        radio_wrapchar = Gtk.RadioButton.new_with_label_from_widget(
            radio_wrapnone, "Character Wrapping")
        self.grid.attach_next_to(radio_wrapchar, radio_wrapnone,
            Gtk.PositionType.RIGHT, 1, 1)

        radio_wrapword = Gtk.RadioButton.new_with_label_from_widget(
            radio_wrapnone, "Word Wrapping")
        self.grid.attach_next_to(radio_wrapword, radio_wrapchar,
            Gtk.PositionType.RIGHT, 1, 1)

        radio_wrapnone.connect("toggled", self.on_wrap_toggled,
            Gtk.WrapMode.NONE)
        radio_wrapchar.connect("toggled", self.on_wrap_toggled,
            Gtk.WrapMode.CHAR)
        radio_wrapword.connect("toggled", self.on_wrap_toggled,
            Gtk.WrapMode.WORD)

    def on_button_clicked(self, widget, tag):
        bounds = self.textbuffer.get_selection_bounds()
        if len(bounds) != 0:
            start, end = bounds
            self.textbuffer.apply_tag(tag, start, end)

    def on_clear_clicked(self, widget):
        start = self.textbuffer.get_start_iter()
        end = self.textbuffer.get_end_iter()
        self.textbuffer.remove_all_tags(start, end)

    def on_editable_toggled(self, widget):
        self.textview.set_editable(widget.get_active())

    def on_cursor_toggled(self, widget):
        self.textview.set_cursor_visible(widget.get_active())

    def on_wrap_toggled(self, widget, mode):
        self.textview.set_wrap_mode(mode)

    def on_justify_toggled(self, widget, justification):
        self.textview.set_justification(justification)

    def on_search_clicked(self, widget):
        dialog = SearchDialog(self)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            cursor_mark = self.textbuffer.get_insert()
            start = self.textbuffer.get_iter_at_mark(cursor_mark)
            if start.get_offset() == self.textbuffer.get_char_count():
                start = self.textbuffer.get_start_iter()

            self.search_and_mark(dialog.entry.get_text(), start)

        dialog.destroy()

    def search_and_mark(self, text, start):
        end = self.textbuffer.get_end_iter()
        match = start.forward_search(text, 0, end)

        if match is not None:
            match_start, match_end = match
            self.textbuffer.apply_tag(self.tag_found, match_start, match_end)
            self.search_and_mark(text, match_end)



win = koolEdi() # a pretty cool text editor
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
